"""
    flask_safe_caching.backends
    ~~~~~~~~~~~~~~~~~~~~~~

    Various caching backends.

    :copyright: (c) 2018 by Peter Justin.
    :copyright: (c) 2010 by Thadeus Burgess.
    :license: BSD, see LICENSE for more details.
"""
from flask_safe_caching.backends.filesystemcache import FileSystemCache
from flask_safe_caching.backends.nullcache import NullCache
from flask_safe_caching.backends.rediscache import RedisCache
from flask_safe_caching.backends.rediscache import RedisClusterCache
from flask_safe_caching.backends.rediscache import RedisSentinelCache
from flask_safe_caching.backends.simplecache import SimpleCache

# TODO: Rename to "redis" when python2 support is removed


__all__ = (
    "null",
    "simple",
    "filesystem",
    "redis",
    "redissentinel",
    "rediscluster",
)


def null(app, config, args, kwargs):
    return NullCache.factory(app, config, args, kwargs)


def simple(app, config, args, kwargs):
    return SimpleCache.factory(app, config, args, kwargs)


def filesystem(app, config, args, kwargs):
    return FileSystemCache.factory(app, config, args, kwargs)


def redis(app, config, args, kwargs):
    return RedisCache.factory(app, config, args, kwargs)


def redissentinel(app, config, args, kwargs):
    return RedisSentinelCache.factory(app, config, args, kwargs)


def rediscluster(app, config, args, kwargs):
    return RedisClusterCache.factory(app, config, args, kwargs)
