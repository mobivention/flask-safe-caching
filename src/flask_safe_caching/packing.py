import msgpack

class FileSystemValue:
    def __init__(self, timeout, value):
        self.timeout = timeout
        self.value = value

    def to_dict(self):
        return {
            "timeout": self.timeout,
            "value": self.value
        }

    @staticmethod
    def from_dict(d):
        return FileSystemValue(d["timeout"], d["value"])


def decode_fsv(obj):
    if '__FileSystemValue__' in obj:
        return FileSystemValue.from_dict(obj["value"])
    return obj


def encode_fsv(obj):
    if isinstance(obj, FileSystemValue):
        return {"__FileSystemValue__": True, "value": obj.to_dict()}
    return obj


def pack(value) -> bytes:
    if isinstance(value, tuple):
        return b"t" + msgpack.packb(value)
    v = msgpack.packb(value, default=encode_fsv)
    return v

def unpack(value: bytes):
    if value.startswith(b"t"):
        l = msgpack.unpackb(value[1:])
        return tuple(l)
    return msgpack.unpackb(value, object_hook=decode_fsv)


