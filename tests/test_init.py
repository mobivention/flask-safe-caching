import pytest
from flask import Flask

from flask_safe_caching import Cache
from flask_safe_caching.backends import FileSystemCache
from flask_safe_caching.backends import NullCache
from flask_safe_caching.backends import RedisCache
from flask_safe_caching.backends import RedisSentinelCache
from flask_safe_caching.backends import SimpleCache


@pytest.fixture
def app():
    app_ = Flask(__name__)

    return app_


@pytest.mark.parametrize(
    "cache_type",
    (
        FileSystemCache,
        NullCache,
        RedisCache,
        RedisSentinelCache,
        SimpleCache,
    ),
)
def test_init_nullcache(cache_type, app, tmp_path):
    extra_config = {
        FileSystemCache: {
            "CACHE_DIR": tmp_path,
        },
    }
    app.config["CACHE_TYPE"] = "flask_safe_caching.backends." + cache_type.__name__
    app.config.update(extra_config.get(cache_type, {}))
    cache = Cache(app=app)

    assert isinstance(app.extensions["cache"][cache], cache_type)
