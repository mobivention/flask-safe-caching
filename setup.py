from setuptools import setup

# Metadata goes in setup.cfg. These are here for GitHub's dependency graph.
setup(
    name="Flask-Safe-Caching",
    install_requires=["Flask", "msgpack"],
    tests_require=[
        "pytest",
        "pytest-xprocess",
        "redis",
    ],
)
